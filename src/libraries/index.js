import React from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {
  BottomTabBar,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import moment from 'moment';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import * as indicator from 'react-native-indicator';
import { WebView } from 'react-native-webview';
import FastImage from 'react-native-fast-image';

export {
  React,
  FontAwesome5,
  AsyncStorage,
  axios,
  _,
  PropTypes,
  createBottomTabNavigator,
  moment,
  SkeletonPlaceholder,
  indicator,
  WebView,
  FastImage,
  BottomTabBar,
};

export * from 'react';
export * from 'react-native';
export * from '@react-navigation/native';

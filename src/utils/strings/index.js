const appString = {
  general: {
    button: {
      login: 'LOGIN',
      register: 'REGISTER',
    },
  },
  pages: {
    home: {
      title: 'Headline News',
    },
    search: {
      placeholderText: 'Search News',
    },
    newsDetail: {
      title: 'News Detail',
    },
    landing: {
      title: 'To Do App',
      subtitle: 'Manage your Task on The GO\ncreated by CIP',
    },
    login: {
      title: 'Enter your credential here',
      footer1: "Don't have account?",
      footer2: 'Register now',
    },
    register: {
      title: 'Please input your profile here',
      footer1: 'Already have account?',
      footer2: 'Sign In',
    },
  },
};

export default appString;

/**
 * @name Fonts
 */

export const FONTS = {
  quickSand: 'Quicksand-Medium',
  quickSandBold: 'Quicksand-Bold',
};

const searchImage = require('./search.png');
const placeholderImage = require('./placeholderImage.png');
const IMG = {
  searchImage,
  placeholderImage,
};
export default IMG;

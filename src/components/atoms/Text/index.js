import React from 'react';
import { View, Text as RNText } from 'react-native';
import { FONTS } from 'utils/styles';
import PropTypes from 'prop-types';

const Text = ({ children, bold, ...props }) => {
  return (
    <RNText
      {...props}
      style={{
        ...props.style,
        fontFamily: bold ? FONTS.quickSandBold : FONTS.quickSand,
      }}>
      {children}
    </RNText>
  );
};

Text.propTypes = {
  bold: PropTypes.bool,
};

Text.defaultProps = {
  bold: false,
};

export default Text;

import React from 'react';
import {
  View,
  Text,
  Pressable,
  TouchableOpacity,
  ActivityIndicator,
} from 'libraries';
import PropTypes from 'prop-types';
import styles from './style';

const Button = (props) => {
  const {
    title,
    onPress,
    backgroundColor,
    textColor,
    isLoading,
    loadingColor,
  } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.button, { backgroundColor }]}>
        {isLoading ? (
          <ActivityIndicator size="small" color={loadingColor} />
        ) : (
          <Text style={[styles.text, { color: textColor }]}>{title}</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  isLoading: PropTypes.bool,
  loadingColor: PropTypes.string,
  backgroundColor: PropTypes.string,
};

Button.defaultProps = {
  title: '',
  onPress: () => {},
  isLoading: false,
  loadingColor: 'black',
  backgroundColor: 'white',
};

export default React.memo(Button);

import {
  createStackNavigator,
  TransitionSpecs,
  TransitionPresets,
} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import {
  HomePage,
  SearchPage,
  NewsDetail,
  CategoryPage,
  AllTaskPage,
  ActiveTaskPage,
  CompletedTaskPage,
  ProfilePage,
  AddTaskPage,
  LandingPage,
  LoginPage,
  RegisterPage,
  SplashScreen,
} from 'pages';

import {
  React,
  createBottomTabNavigator,
  FontAwesome5,
  View,
  BottomTabBar,
} from 'libraries';
import { Color, FONTS } from 'utils';
import { RFC_2822 } from 'moment';
import FabButton from 'components/svg/fab';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-eva-icons';

const Stack = createStackNavigator();

const EmptyScreen = () => {
  return null;
};

/*--------------------------
     Transition Options
---------------------------
*/

const CustomTransition = {
  headerShown: false,
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec,
  },
  ...TransitionPresets.SlideFromRightIOS,
};

const BottomTab = createBottomTabNavigator();

/*--------------------------
     Bottom Navigation
---------------------------
*/

function BottomNavigation() {
  return (
    <BottomTab.Navigator
      initialRouteName="AllTaskPage"
      tabBar={(props) => (
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.22,
            shadowRadius: 2.22,
          }}>
          <BottomTabBar {...props} />
        </View>
      )}
      tabBarOptions={{
        activeTintColor: '#24B07D',
        activeBackgroundColor: Color.primaryColor,
        labelStyle: {
          fontFamily: FONTS.quickSand,
        },
        style: {
          borderTopWidth: 0,
          backgroundColor: 'transparent',
          elevation: 30,
        },
        tabStyle: {
          backgroundColor: 'white',
        },
      }}>
      <BottomTab.Screen
        name="AllTaskPage"
        component={AllTaskPage}
        options={{
          tabBarLabel: 'All Task',
          tabBarIcon: (tabInfo) => (
            // <FontAwesome5 name={'tasks'} color={tabInfo.color} size={20} />
            <Icon name="home" width={24} height={24} fill={tabInfo.color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="ActiveTaskPage"
        component={ActiveTaskPage}
        options={{
          tabBarLabel: 'Active Task',
          tabBarIcon: (tabInfo) => (
            <FontAwesome5
              name={'clipboard-list'}
              color={tabInfo.color}
              size={20}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Add task"
        component={EmptyScreen}
        options={({ navigation }) => ({
          tabBarButton: () => (
            <View
              style={{
                position: 'relative',
                width: 75,
                alignItems: 'center',
              }}>
              <FabButton
                color={'white'}
                style={{
                  position: 'absolute',
                  top: 0,
                }}
              />
              <TouchableOpacity
                style={{
                  top: -22.5,
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 50,
                  height: 50,
                  borderRadius: 27,
                  backgroundColor: 'white',
                }}
                onPress={() => {
                  navigation.navigate('Add Task');
                }}>
                <FontAwesome5 name={'plus'} color={'green'} size={25} />
              </TouchableOpacity>
            </View>
          ),
        })}
      />
      <BottomTab.Screen
        name="CompletedTaskPage"
        component={CompletedTaskPage}
        options={{
          tabBarLabel: 'Completed Task',
          tabBarIcon: (tabInfo) => (
            <Icon
              name="checkmark-square"
              width={24}
              height={24}
              fill={tabInfo.color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="ProfilePage"
        component={ProfilePage}
        options={{
          tabBarLabel: 'My Profile',
          tabBarIcon: (tabInfo) => (
            <Icon name="person" width={24} height={24} fill={tabInfo.color} />
          ),
        }}
      />

      {/* <BottomTab.Screen
        name="Home"
        component={HomePage}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: (tabInfo) => (
            <FontAwesome5 name={'home'} color={tabInfo.color} size={20} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Search"
        component={SearchPage}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: (tabInfo) => (
            <FontAwesome5 name={'search'} color={tabInfo.color} size={20} />
          ),
        }}
      /> */}
    </BottomTab.Navigator>
  );
}

/*--------------------------
     Routes
---------------------------
*/

class Routes extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Splash"
          screenOptions={CustomTransition}>
          <Stack.Screen name="Splash" component={SplashScreen} />
          <Stack.Screen name="Welcome" component={BottomNavigation} />
          <Stack.Screen name="NewsDetail" component={NewsDetail} />
          <Stack.Screen name="Landing" component={LandingPage} />
          <Stack.Screen name="Login" component={LoginPage} />
          <Stack.Screen name="Register" component={RegisterPage} />
          <Stack.Screen name="Add Task" component={AddTaskPage} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Routes;

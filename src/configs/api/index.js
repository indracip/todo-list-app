import ApiRequest from './config';
import baseUrl from './url';

const API = {};

API.searchNewsN = ApiRequest.get(baseUrl.news_api.searchNews);
API.headline = ApiRequest.get(baseUrl.news_api.headline);
API.register = ApiRequest.post(baseUrl.todo.register);
API.login = ApiRequest.post(baseUrl.todo.login);
API.logout = ApiRequest.post(baseUrl.todo.logout);

API.myProfile = ApiRequest.get(baseUrl.todo.myProfile);
API.updateMyProfile = ApiRequest.put(baseUrl.todo.myProfile);
API.deleteMyProfile = ApiRequest.delete(baseUrl.todo.myProfile);

API.addTask = ApiRequest.post(baseUrl.todo.task);
API.getTask = ApiRequest.get(baseUrl.todo.task);
API.updateTask = ApiRequest.put(baseUrl.todo.task);
API.deleteTask = ApiRequest.delete(baseUrl.todo.task);

export default API;

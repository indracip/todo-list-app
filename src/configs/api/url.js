const base_url_news = 'https://newsapi.org/v2';
const base_url_todo = 'https://api-nodejs-todolist.herokuapp.com';

const baseUrl = {
  news_api: {
    headline: `${base_url_news}/top-headlines`,
    searchNews: `${base_url_news}/everything`,
  },
  todo: {
    register: `${base_url_todo}/user/register`,
    login: `${base_url_todo}/user/login`,
    logout: `${base_url_todo}/user/logout`,
    myProfile: `${base_url_todo}/user/me`,
    task: `${base_url_todo}/task`,
  },
};

export default baseUrl;

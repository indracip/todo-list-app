const defaultState = {
  completedTask: 0,
  activeTask: 0,
  dataAllTask: [],
  dataActiveTask: [],
  dataCompletedTask: [],
  loadingAll: false,
  loadingActive: false,
  loadingCompleted: false,
  loadingUpdate: false,
  successUpdate: null,
  loadingDelete: false,
  successDelete: null,
  loadingAdd: false,
  successAdd: null,
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'UPDATE_COMPLETE_TASK': {
      return {
        ...state,
        completedTask: action.summary,
        dataCompletedTask: action.payload,
      };
    }

    case 'UPDATE_ACTIVE_TASK': {
      return {
        ...state,
        activeTask: action.summary,
        dataActiveTask: action.payload,
        loadingActive: false,
      };
    }

    case 'FETCH_ALL_TASK': {
      return {
        ...state,
        dataAllTask: action.all,
        dataActiveTask: action.active,
        dataCompletedTask: action.completed,
        completedTask: action.completed.length,
        activeTask: action.active.length,
        loadingAll: false,
        loadingActive: false,
        loadingCompleted: false,
      };
    }

    case 'FETCH_ACTIVE_TASK': {
      return {
        ...state,
        dataActiveTask: action.payload,
      };
    }

    case 'FETCH_COMPLETED_TASK': {
      return {
        ...state,
        dataCompletedTask: action.payload,
      };
    }

    case 'GETTING_ALL_TASK': {
      return {
        ...state,
        loadingAll: true,
      };
    }

    case 'GETTING_ACTIVE_TASK': {
      return {
        ...state,
        loadingActive: true,
      };
    }

    case 'GETTING_COMPLETED_TASK': {
      return {
        ...state,
        loadingCompleted: true,
      };
    }

    case 'POSTING_UPDATE_TASK': {
      return {
        ...state,
        loadingUpdate: action.payload,
      };
    }

    case 'REQ_DELETE_TASK': {
      return {
        ...state,
        loadingDelete: action.payload,
      };
    }

    case 'REQ_ADD_TASK': {
      return {
        ...state,
        loadingAdd: action.payload,
      };
    }

    case 'UPDATE_STATUS': {
      return {
        ...state,
        successUpdate: action.payload,
      };
    }

    case 'DELETE_STATUS': {
      return {
        ...state,
        successDelete: action.payload,
      };
    }

    case 'ADD_STATUS': {
      return {
        ...state,
        successAdd: action.payload,
      };
    }

    case 'LOGOUT': {
      return { ...defaultState };
    }

    default:
      return state;
  }
};

const defaultState = {
  name: '',
  email: '',
  age: '',
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'GETTING_PROFILE': {
      return state;
    }

    case 'DELETE_PROFILE': {
      return state;
    }

    case 'GET_PROFILE_INFO': {
      return {
        ...state,
        name: action.payload.name,
        email: action.payload.email,
        age: action.payload.age,
      };
    }
    case 'LOGOUT': {
      return { ...defaultState };
    }
    default:
      return state;
  }
};

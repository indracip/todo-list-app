import { combineReducers } from 'redux';

import authStoreReducer from './authStoreReducer';
import taskReducer from './taskReducer';
import profileReducer from './profileReducer';

const reducers = {
  authStore: authStoreReducer,
  taskStore: taskReducer,
  profileStore: profileReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;

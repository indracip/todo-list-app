import API from 'configs/api';
import { AsyncStorage } from 'libraries';

export default function getProfile(payload = {}) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    payload.headers = {
      Authorization: `Bearer ${token}`,
    };
    dispatch({
      type: 'GETTING_PROFILE',
    });

    try {
      const result = await API.myProfile(payload);

      if (result) {
        await dispatch({
          type: 'GET_PROFILE_INFO',
          payload: result,
        });
      } else {
        alert(`Error Fetching Data ${type} tasks!`);
      }
    } catch (err) {
      console.log(err);
      alert(`Error Fetching Data ${type} tasks!\nError: ${err}`);
    }
  };
}

export function deleteProfile(payload = {}) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    payload.headers = {
      Authorization: `Bearer ${token}`,
    };
    dispatch({
      type: 'DELETE_PROFILE',
    });

    try {
      const result = await API.deleteMyProfile(payload);

      if (result) {
        //   await dispatch({
        //     type: 'GET_PROFILE_INFO',
        //     payload: result,
        //   });
      } else {
        alert(`Error Fetching Data ${type} tasks!`);
      }
    } catch (err) {
      console.log(err);
      alert(`Error Fetching Data ${type} tasks!\nError: ${err}`);
    }
  };
}

export function clearProfile() {
  return async (dispatch) => {
    await dispatch({
      type: 'LOGOUT',
    });
  };
}

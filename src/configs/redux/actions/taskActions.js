import { AsyncStorage } from 'libraries';
import API from 'configs/api';

export default function getAllTask(payload = {}) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    payload.headers = {
      Authorization: `Bearer ${token}`,
    };
    dispatch({
      type: 'GETTING_ALL_TASK',
    });

    try {
      console.log('mulai hit');

      const result = await API.getTask(payload);

      if (result) {
        const completed = result.data.filter((item) => item.completed === true);
        const active = result.data.filter((item) => item.completed === false);
        dispatch({
          type: 'FETCH_ALL_TASK',
          all: result.data,
          completed: completed,
          active: active,
        });
      } else {
        alert(`Error Fetching Data all tasks!`);
      }
    } catch (err) {
      console.log(err);
      alert(`Error Fetching Data all tasks!\nError: ${err}`);
    }
  };
}

export function updateTask(payload = {}) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    payload.headers = {
      Authorization: `Bearer ${token}`,
    };
    dispatch({
      type: 'POSTING_UPDATE_TASK',
      payload: true,
    });

    try {
      const result = await API.updateTask(payload);
      if (result) {
        dispatch({
          type: 'POSTING_UPDATE_TASK',
          payload: false,
        });
        dispatch({
          type: 'UPDATE_STATUS',
          payload: true,
        });

        dispatch(getAllTask());
      } else {
        dispatch({
          type: 'UPDATE_STATUS',
          payload: false,
        });
      }
    } catch (err) {
      dispatch({
        type: 'UPDATE_STATUS',
        payload: false,
      });
    }
  };
}

export function deleteTask(payload = {}) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    payload.headers = {
      Authorization: `Bearer ${token}`,
    };
    dispatch({
      type: 'REQ_DELETE_TASK',
      payload: true,
    });

    try {
      const result = await API.deleteTask(payload);
      if (result) {
        dispatch({
          type: 'REQ_DELETE_TASK',
          payload: false,
        });
        dispatch({
          type: 'DELETE_STATUS',
          payload: true,
        });

        dispatch(getAllTask());
      } else {
        dispatch({
          type: 'DELETE_STATUS',
          payload: false,
        });
      }
    } catch (err) {
      dispatch({
        type: 'DELETE_STATUS',
        payload: false,
      });
    }
  };
}

export function addTask(payload = {}) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    payload.headers = {
      Authorization: `Bearer ${token}`,
    };
    dispatch({
      type: 'REQ_ADD_TASK',
      payload: true,
    });

    try {
      const result = await API.addTask(payload);
      if (result) {
        dispatch({
          type: 'REQ_ADD_TASK',
          payload: false,
        });
        dispatch({
          type: 'ADD_STATUS',
          payload: true,
        });

        dispatch(getAllTask());
      } else {
        dispatch({
          type: 'ADD_STATUS',
          payload: false,
        });
      }
    } catch (err) {
      dispatch({
        type: 'ADD_STATUS',
        payload: false,
      });
    }
  };
}

export function updateStatus(type, status) {
  return async (dispatch) => {
    switch (type) {
      case 'update': {
        dispatch({
          type: 'UPDATE_STATUS',
          payload: status,
        });
      }
      case 'delete': {
        dispatch({
          type: 'DELETE_STATUS',
          payload: status,
        });
      }

      case 'add': {
        dispatch({
          type: 'ADD_STATUS',
          payload: status,
        });
      }
      default:
    }
  };
}
export function clearTask() {
  return async (dispatch) => {
    await dispatch({
      type: 'LOGOUT',
    });
  };
}

import API from 'configs/api';
import { AsyncStorage } from 'libraries';
import { clearProfile } from './profileActions';
import { clearTask } from './taskActions';

export default function fetchLogin(payload) {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_LOGIN_REQUEST',
    });

    try {
      API.login(payload);
      const result = await API.login(payload);

      if (result) {
        dispatch({
          type: 'FETCH_LOGIN_SUCCESS',
          payload: result,
        });
      } else {
        dispatch({
          type: 'FETCH_LOGIN_FAILED',
          error: result,
        });
      }
    } catch (err) {
      dispatch({
        type: 'FETCH_LOGIN_FAILED',
        error: err,
      });
    }
  };
}

export function logout() {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    const payload = {};
    payload.headers = {
      Authorization: `Bearer ${token}`,
    };
    try {
      const result = await API.logout(payload);
      if (result) {
        await AsyncStorage.removeItem('token');
        dispatch({
          type: 'LOGOUT',
        });
        // dispatch(clearProfile());
        // dispatch(clearTask());
      } else {
        console.log('logout from server failed');
      }
    } catch (e) {}
  };
}

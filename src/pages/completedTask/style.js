import { StyleSheet } from 'libraries';
import { Color } from 'utils';
const styles = StyleSheet.create({
  loadmore: { alignSelf: 'center' },
});

export default styles;

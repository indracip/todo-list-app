import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { scale, Color, FONTS } from 'utils';
import { Icon } from 'react-native-eva-icons';
import { Text, Button } from 'components';
import {
  View,
  StatusBar,
  Dimensions,
  moment,
  useState,
  useEffect,
  FlatList,
  SkeletonPlaceholder,
  indicator,
  TouchableHighlight,
  TouchableOpacity,
  useRef,
} from 'libraries';
import API from 'configs/api';
import styles from './style';
import { connect } from 'react-redux';
import getTask, {
  deleteTask,
  updateStatus,
  updateTask,
} from 'configs/redux/actions/taskActions';
import Modal from 'react-native-modal';
import { TextInput } from 'react-native-gesture-handler';
import DropDownPicker from 'react-native-dropdown-picker';
import { showMessage, hideMessage } from 'react-native-flash-message';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';

const CompletedTaskPage = (props) => {
  const {
    dispatchTask,
    dataCompletedTask,
    loadingAll,
    dispatchUpdateTask,
    successUpdate,
    dispatchUpdateStatus,
    loadingUpdate,
    dispatchDeleteTask,
    loadingDelete,
    successDelete,
    successAdd,
  } = props;
  // const [offset, setOffset] = useState(1);
  // const [loading, setLoading] = useState(false);
  // const [dataCompletedTask, setdataCompletedTask] = useState([]);
  const [endList, setEndList] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [dataUpdateModal, setDataUpdateModal] = useState({
    description: 'Ini inisial data',
    status: false,
    selectedId: null,
  });

  const [dataCompletedTaskN, setDataCompletedTaskN] = useState([]);
  const dataListRef = useRef(null);

  useEffect(() => {
    setTimeout(function () {
      dataListRef.current ? dataListRef.current.scrollToEnd() : null;
    }, 1500);
  }, [successAdd]);

  const toggleModal = () => {
    console.log('toogle');
    setModalVisible(!isModalVisible);
  };

  let onEndReachedCalledDuringMomentum = true;
  useEffect(() => {
    setDataCompletedTaskN(
      dataCompletedTask.filter((item) => item.completed == true),
    );
  }, [dataCompletedTask]);

  useEffect(() => {
    if (successUpdate == true) {
      showMessage({
        message: 'Success',
        description: 'Your task has been updated',
        type: 'success',
      });
      setTimeout(function () {
        hideMessage();
        console.log('udh 3 detik');
      }, 3000);
    } else if (successUpdate == false) {
      showMessage({
        message: 'Ups',
        description: 'Sorry, updating task failed',
        type: 'danger',
      });
    }
    dispatchUpdateStatus('update', null);

    setModalVisible(false);
  }, [successUpdate]);

  useEffect(() => {
    if (successDelete == true) {
      showMessage({
        message: 'Success',
        description: 'Your task has been deleted',
        type: 'success',
      });
      setTimeout(function () {
        hideMessage();
        console.log('udh 3 detik');
      }, 3000);
    } else if (successDelete == false) {
      showMessage({
        message: 'Ups',
        description: 'Sorry, deleting task failed',
        type: 'danger',
      });
    }
    dispatchUpdateStatus('update', null);

    setModalVisible(false);
  }, [successDelete]);

  // const getAllTask = async ({ isReset }) => {
  //   setLoading(true);
  //   try {
  //     const payload = {
  //       headers: {
  //         Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDAwZjdkNmRhZDIwNjAwMTc1YmY2YzEiLCJpYXQiOjE2MTA4NDYzNjh9.dizCvpjWlzqdBKlyAfGzzIl0IhB3zlFMIt4yEr8nFHc`,
  //       },
  //     };
  //     const data = await API.getTask(payload);
  //     if (data) {
  //       setOffset(offset + 1);
  //       if (data.count === 0) {
  //         setEndList(true);
  //       } else if (isReset) {
  //         setdataCompletedTask(data.data);
  //       } else {
  //         setdataCompletedTask([...dataCompletedTask, ...data.data]);
  //       }
  //       setLoading(false);
  //     }
  //   } catch (e) {
  //     if (e.error) {
  //       alert(e.error);
  //       navigation.navigate('Login');
  //     }
  //   }
  // };

  const getAllTask = async () => {
    try {
      const payload = {
        params: {
          completed: true,
        },
      };
      dispatchTask('complete', payload);
    } catch (e) {
      // alert('Error: ', e);
      console.log(e);
    }
  };

  const renderFooter = () => {
    return loadingAll && !endList ? (
      <View style={styles.loadmore}>
        <indicator.DotsLoader color={Color.primaryColor} />
      </View>
    ) : (
      <View></View>
    );
  };

  const renderItem = ({ item }) => {
    const day = moment(new Date(item.updatedAt)).format('MMM Do YY');
    const time = moment(new Date(item.updatedAt)).format('LT');
    return (
      <TouchableOpacity
        onPress={() => {
          console.log(item.completed);
          setDataUpdateModal({
            description: item.description,
            status: item.completed,
            selectedId: item._id,
          });
          toggleModal();
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: scale(10),
          }}>
          <View
            style={{
              width: scale(5),
              backgroundColor: item.completed ? '#24B07D' : 'orange',
              marginRight: scale(15),
            }}></View>
          <View style={{ flex: 1 }}>
            <Text bold style={{ fontSize: scale(15) }}>
              {item.description}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: scale(5),
              }}>
              <View
                style={{
                  //   padding: scale(8),
                  backgroundColor: '#EAF9F2',
                  borderRadius: scale(5),
                }}>
                <Text
                  bold
                  style={{
                    marginVertical: scale(5),
                    marginHorizontal: scale(10),
                    color: '#24B07D',
                  }}>
                  {day}
                </Text>
              </View>
              <View style={{ marginLeft: scale(7) }}>
                <Text bold style={{ color: '#AAB9B3' }}>
                  {time}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  const today = moment(new Date()).format('dddd D');
  return (
    <SafeAreaView
      style={{
        flex: 1,
        paddingHorizontal: scale(15),
        backgroundColor: '#FBFAFF',
      }}>
      <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
      <View style={{ marginBottom: scale(20) }}>
        <Text style={{ fontSize: scale(30) }}>Completed Task</Text>
        <Text bold style={{ fontSize: scale(25), color: 'green' }}>
          {today}
        </Text>
      </View>

      {dataCompletedTaskN.length > 0 ? (
        <FlatList
          ref={dataListRef}
          data={dataCompletedTaskN}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={renderFooter}
          onMomentumScrollBegin={() => {
            onEndReachedCalledDuringMomentum = false;
          }}
          onEndReached={() => {
            if (!onEndReachedCalledDuringMomentum) {
              // getAllTask({ isReset: false });
              onEndReachedCalledDuringMomentum = true;
            }
          }}
          onEndReachedThreshold={0.2}
        />
      ) : (
        Array.from({ length: 10 }).map((_, index) => (
          <View key={index} style={{ marginBottom: 15, padding: 10 }}>
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item flexDirection="row">
                <SkeletonPlaceholder.Item
                  flex={1}
                  paddingHorizontal={10}
                  justifyContent={'space-between'}>
                  <SkeletonPlaceholder.Item
                    width="100%"
                    height={180}
                    borderRadius={6}
                  />
                </SkeletonPlaceholder.Item>
              </SkeletonPlaceholder.Item>
            </SkeletonPlaceholder>
          </View>
        ))
      )}

      <Modal isVisible={isModalVisible} backdropTransitionOutTiming={0}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: scale(10),
            padding: scale(10),
            position: 'relative',
          }}>
          <Text bold style={{ fontSize: 25, marginBottom: scale(5) }}>
            Updating Data
          </Text>
          <Text style={{ fontSize: 20, marginBottom: scale(10) }}>
            Description
          </Text>
          <View
            style={{
              paddingLeft: 10,
              marginBottom: scale(24),
              backgroundColor: '#EAEAEA',
              borderWidth: 0,
              borderRadius: 10,
              height: scale(100),
            }}>
            <AutoGrowingTextInput
              value={dataUpdateModal.description}
              onChange={(event) => {
                setDataUpdateModal({
                  ...dataUpdateModal,
                  description: event.nativeEvent.text,
                });
              }}
              placeholder={'Input Description'}
              placeholderTextColor="#66737C"
              style={{
                fontFamily: FONTS.quickSand,
                fontSize: scale(15),
              }}
              enableScrollToCaret
            />
          </View>

          <Text style={{ fontSize: 20, marginBottom: scale(10) }}>Status</Text>
          <DropDownPicker
            placeholder={'Select your task status'}
            selectedLabelStyle={{
              color: 'green',
            }}
            activeLabelStyle={{
              color: 'green',
            }}
            labelStyle={{
              color: 'black',
            }}
            items={[
              {
                label: 'Completed',
                value: true,
                icon: () => (
                  <Icon
                    name="checkmark-square"
                    width={18}
                    height={18}
                    fill={'green'}
                  />
                ),
              },
              {
                label: 'Active',
                value: false,
                icon: () => (
                  <Icon name="list" width={18} height={18} fill={'green'} />
                ),
              },
            ]}
            defaultValue={dataUpdateModal.status}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: '#fafafa' }}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            dropDownStyle={{ backgroundColor: '#fafafa' }}
            onChangeItem={(item) =>
              setDataUpdateModal({
                ...dataUpdateModal,
                status: item.value,
              })
            }
          />
          <View
            style={{
              flexDirection: 'row',
              marginTop: scale(30),
              justifyContent: 'space-between',
            }}>
            <View style={{ width: Dimensions.get('window').width * 0.4 }}>
              <Button
                backgroundColor={'#24B07D'}
                textColor={'white'}
                title="Update"
                onPress={() => {
                  console.log('tombol update');
                  console.log(dataUpdateModal.selectedId);
                  const payload = {
                    paramsId: dataUpdateModal.selectedId,
                    body: {
                      completed: dataUpdateModal.status,
                      description: dataUpdateModal.description,
                    },
                  };
                  dispatchUpdateTask(payload);
                }}
                isLoading={loadingUpdate}
                loadingColor={'white'}
              />
            </View>
            <View style={{ width: Dimensions.get('window').width * 0.4 }}>
              <Button
                backgroundColor={'red'}
                textColor={'white'}
                title="Delete"
                onPress={() => {
                  const payload = {
                    paramsId: dataUpdateModal.selectedId,
                  };
                  dispatchDeleteTask(payload);
                }}
                isLoading={loadingDelete}
                loadingColor={'white'}
              />
            </View>
          </View>
          <View
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              padding: scale(10),
            }}>
            <TouchableOpacity
              onPress={() => {
                console.log('mau pencet');
                toggleModal();
              }}>
              <Icon
                name="close-outline"
                width={24}
                height={24}
                fill={'black'}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    dataCompletedTask: state.taskStore.dataCompletedTask,
    loadingAll: state.taskStore.loadingAll,
    completedTask: state.taskStore.completedTask,
    activeTask: state.taskStore.activeTask,
    successUpdate: state.taskStore.successUpdate,
    successDelete: state.taskStore.successDelete,
    loadingUpdate: state.taskStore.loadingUpdate,
    loadingDelete: state.taskStore.loadingDelete,
    successAdd: state.taskStore.successAdd,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchTask: (type, payload) => dispatch(getTask(type, payload)),
    dispatchUpdateTask: (payload) => dispatch(updateTask(payload)),
    dispatchUpdateStatus: (type, payload) =>
      dispatch(updateStatus(type, payload)),
    dispatchDeleteTask: (payload) => dispatch(deleteTask(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CompletedTaskPage);

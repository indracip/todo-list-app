import { StyleSheet } from 'libraries';

const styles = StyleSheet.create({
  container: { flex: 1, position: 'relative' },
  navbar: {
    height: 70,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  navbarContent: {
    flexDirection: 'row',
    marginHorizontal: 15,
    alignItems: 'center',
  },
  navbarTitle: {
    marginHorizontal: 15,
    fontSize: 25,
    textAlign: 'center',
    flex: 1,
  },
  loading: {
    position: 'absolute',
    backgroundColor: 'transparent',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default styles;

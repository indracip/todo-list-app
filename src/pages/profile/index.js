import React from 'react';
import { View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { scale } from 'utils';
import { Button, Text } from 'components';
import { Icon } from 'react-native-eva-icons';
import { connect } from 'react-redux';
import getProfile, {
  deleteProfile,
} from 'configs/redux/actions/profileActions';
import { useEffect } from 'libraries';
import { logout } from 'configs/redux/actions/authActions';

const ProfilePage = (props) => {
  const {
    completedTask,
    activeTask,
    name,
    email,
    age,
    dispatchProfile,
    navigation,
    dispatchLogout,
    dispatchDelete,
  } = props;
  useEffect(() => {
    dispatchProfile();
  }, []);
  return (
    <View
      style={{
        backgroundColor: '#FBFAFF',
        flex: 1,
        position: 'relative',
      }}>
      <View
        style={{
          paddingBottom: scale(35),
          borderBottomLeftRadius: scale(25),
          borderBottomRightRadius: scale(25),
          backgroundColor: 'white',
          zIndex: 99,
        }}>
        <View style={{ flexDirection: 'row' }}>
          <Text
            bold
            style={{
              fontSize: scale(30),
              textAlign: 'left',
              flex: 1,
              paddingLeft: scale(10),
            }}>
            My Profile
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Login');
              dispatchLogout();
            }}>
            <View style={{ padding: scale(10) }}>
              <Icon
                name="log-out"
                width={28}
                height={28}
                fill={'black'}
                solid
              />
            </View>
          </TouchableOpacity>
        </View>

        <Image
          source={{ uri: 'https://emoji.gg/assets/emoji/PandaSnake.png' }}
          style={{
            height: scale(120),
            width: scale(120),
            marginTop: scale(50),
            alignSelf: 'center',
          }}
        />
        <View
          style={{
            flexDirection: 'row',
          }}>
          <Text
            bold
            style={{
              flex: 1,
              marginTop: scale(10),
              fontSize: scale(25),
              textAlign: 'center',
            }}>
            {name}
          </Text>
        </View>
      </View>
      <View
        style={{
          alignItems: 'center',
          paddingTop: scale(40),
          paddingBottom: scale(15),
          backgroundColor: '#24B07D',
          marginTop: -scale(25),
          borderBottomLeftRadius: scale(25),
          borderBottomRightRadius: scale(25),
        }}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              width: Dimensions.get('window').width * 0.5,
              alignItems: 'center',
            }}>
            <Text
              bold
              style={{
                color: 'white',
                fontSize: scale(25),
              }}>
              {completedTask}
            </Text>
            <Text
              style={{
                color: 'white',
                fontSize: scale(15),
              }}>
              Completed Tasks
            </Text>
          </View>
          <View
            style={{
              width: Dimensions.get('window').width * 0.5,
              alignItems: 'center',
            }}>
            <Text bold style={{ color: 'white', fontSize: scale(25) }}>
              {activeTask}
            </Text>
            <Text
              style={{
                color: 'white',
                fontSize: scale(15),
              }}>
              Active Tasks
            </Text>
          </View>
        </View>
      </View>
      <View style={{ padding: scale(15) }}>
        <Text bold style={{ fontSize: scale(20) }}>
          Email
        </Text>
        <Text style={{ fontSize: scale(15) }}>{email}</Text>
        <Text bold style={{ fontSize: scale(20) }}>
          Age
        </Text>
        <Text style={{ fontSize: scale(15) }}>{age} years old</Text>
      </View>
      <View style={{ marginTop: scale(40) }}>
        <Button
          title={'Delete Profile'}
          backgroundColor={'red'}
          textColor={'white'}
          onPress={() => {
            navigation.navigate('Register');
            dispatchDelete();
          }}
        />
      </View>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    completedTask: state.taskStore.completedTask,
    activeTask: state.taskStore.activeTask,
    name: state.profileStore.name,
    email: state.profileStore.email,
    age: state.profileStore.age,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchProfile: () => dispatch(getProfile()),
    dispatchLogout: () => dispatch(logout()),
    dispatchDelete: () => dispatch(deleteProfile()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);

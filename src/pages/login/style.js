import { scale } from 'utils';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute',
    zIndex: 0,
    width: '100%',
    height: '100%',
  },
  topWrapper: {
    marginTop: 100,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  logo: {
    backgroundColor: 'white',
    borderRadius: 25,
    height: scale(150),
    width: scale(150),
    marginBottom: 15,
  },
  text: {
    title: {
      marginTop: 10,
      color: 'black',
      fontSize: 20,
      fontWeight: 'bold',
    },
    subtitle: {
      marginTop: 20,
      color: 'black',
      fontSize: 18,
      textAlign: 'center',
    },
    footer1: {
      marginTop: 10,
      color: 'black',
      fontSize: 15,
      textAlign: 'center',
    },
    footer2: {
      marginTop: 10,
      color: 'black',
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center',
    },
  },

  bottomWrapper: { marginTop: 80 },
};

export default styles;

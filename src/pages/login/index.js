/* eslint-disable react-native/no-inline-styles */
import { Button } from 'components';
import {
  React,
  Text,
  View,
  Image,
  TextInput,
  Dimensions,
  ScrollView,
  Pressable,
  AsyncStorage,
  useState,
  useEffect,
} from 'libraries';
import { appString } from 'utils';
import styles from './style';
import API from 'configs/api';
import { connect } from 'react-redux';
import fetchLogin from 'configs/redux/actions/authActions';

// class LoginPage extends React.Component {
const LoginPage = (props) => {
  const { navigation, authStore, dispatchLogin } = props;
  const [dataProfile, setDataProfile] = useState({
    email: '',
    password: '',
  });

  useEffect(() => {
    if (authStore.payload.token) {
      AsyncStorage.setItem('token', authStore.payload.token);
      if (authStore.payload.user) {
        AsyncStorage.setItem('user', JSON.stringify(authStore.payload.user));
      }
      navigation.replace('Welcome');
    }
  }, [authStore.payload]);

  const submitLogin = async () => {
    console.log('masuk');
    const { email, password } = dataProfile;
    // const { navigation } = this.props;
    const payload = {
      body: {
        email,
        password,
      },
    };
    try {
      console.log('Login...');
      dispatchLogin(payload);
      // const reqData = await API.login(payload);
      // if (reqData.token) {
      //   AsyncStorage.setItem('token', JSON.stringify(reqData.token));
      //   if (reqData.user) {
      //     AsyncStorage.setItem('user', JSON.stringify(reqData.user));
      //   }
      //   console.log('Login Success');
      //   navigation.replace('Welcome');
      // } else {
      //   alert('Login Failed');
      // }
      // console.log(reqData);
    } catch (e) {
      alert('Error: ', e);
      console.log(e);
    }
  };

  const title = appString.pages.login.title;
  const footer1 = appString.pages.login.footer1;
  const footer2 = appString.pages.login.footer2;
  const backgroundImageUrl = 'https://wallpaperaccess.com/full/2919081.jpg';
  const logoUrl =
    'https://raw.githubusercontent.com/fabiospampinato/vscode-todo-plus/master/resources/logo/logo.png';

  const { email, password } = dataProfile;
  // const { saveToAsync } = this;

  return (
    <View style={styles.container}>
      <Image
        style={styles.backgroundImage}
        source={{
          uri: backgroundImageUrl,
        }}
      />
      <ScrollView>
        <View style={styles.topWrapper}>
          <Image
            style={styles.logo}
            source={{
              uri: logoUrl,
            }}
          />
          <Text style={styles.text.title}>{title}</Text>
          <TextInput
            placeholder="Email"
            style={{
              height: 60,
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              width: Dimensions.get('window').width * 0.8,
            }}
            onChangeText={(text) => {
              console.log(text);
              setDataProfile({
                ...dataProfile,
                email: text,
              });
              // this.setState({
              //   email: text,
              // });
            }}
            value={email}
          />

          <TextInput
            placeholder="Kata Sandi"
            secureTextEntry={true}
            style={{
              height: 60,
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              width: Dimensions.get('window').width * 0.8,
            }}
            onChangeText={(text) => {
              setDataProfile({
                ...dataProfile,
                password: text,
              });
              // this.setState({
              //   password: text,
              // });
            }}
            value={password}
          />
        </View>

        <View style={styles.bottomWrapper}>
          <Button
            title={appString.general.button.login.toUpperCase()}
            onPress={() => submitLogin()}
          />
          <Text style={styles.text.footer1}>{footer1}</Text>
          <Pressable onPress={() => navigation.navigate('Register')}>
            <Text style={styles.text.footer2}>{footer2}</Text>
          </Pressable>
        </View>
      </ScrollView>
    </View>
  );
};
// }

function mapStateToProps(state) {
  return {
    authStore: state.authStore,
    isLoading: state.authStore.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchLogin: (payload) => dispatch(fetchLogin(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

import API from 'configs/api';
import { API_KEY } from 'configs/const';
import {
  React,
  View,
  useState,
  useEffect,
  AppState,
  useRef,
  FlatList,
  Image,
  moment,
  FontAwesome5,
  StatusBar,
  SkeletonPlaceholder,
  indicator,
  Pressable,
  FastImage,
  ScrollView,
} from 'libraries';
import { Text } from 'components/atoms';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import styles from './style';
import { appString, Color } from 'utils';

const HomePage = ({ navigation }) => {
  const appState = useRef(AppState.currentState);
  const [dataNews, setDataNews] = useState([]);
  const [offset, setOffset] = useState(1);
  const [loading, setLoading] = useState(false);
  const [endList, setEndList] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState('all');

  let onEndReachedCalledDuringMomentum = true;
  const dataCategory = [
    'all',
    'business',
    'entertainment',
    'general',
    'health',
    'science',
    'sports',
    'technology',
  ];

  useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);
    getNews({ isReset: false });
    //
    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  useEffect(() => {
    setEndList(false);
    setDataNews([]);
    getNews({ isReset: true });
  }, [selectedCategory]);

  const getNews = async ({ isReset }) => {
    try {
      if (!endList) {
        setLoading(true);
        const payload = {
          params: {
            country: 'id',
            apiKey: API_KEY.news_api,
            pageSize: 10,
            page: isReset ? 0 : offset,
            category: selectedCategory === 'all' ? '' : selectedCategory,
          },
        };
        const data = await API.headline(payload);
        if (data) {
          setOffset(offset + 1);
          if (data.articles.length === 0) {
            setEndList(true);
          } else if (isReset) {
            setDataNews(data.articles);
          } else {
            setDataNews([...dataNews, ...data.articles]);
          }
          setLoading(false);
        }
      }
    } catch (e) {}
  };

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }

    appState.current = nextAppState;
    console.log('AppState', appState.current);
  };

  const renderFooter = () => {
    return loading && !endList ? (
      <View style={styles.loadmore}>
        <indicator.DotsLoader color={Color.primaryColor} />
      </View>
    ) : (
      <View></View>
    );
  };

  const renderItem = ({ item }) => {
    const created_at_format = moment(new Date(item.publishedAt)).format('LLL');
    let imageUrl =
      'https://mtsn4malang.sch.id/wp-content/uploads/2019/08/placeholder-1.png';
    if (item.urlToImage != '' && item.urlToImage != null) {
      imageUrl = item.urlToImage;
    }
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('NewsDetail', {
            url: item.url,
          });
        }}>
        <View style={styles.item}>
          <View style={{ flex: 1 }}>
            <View style={styles.headerWrapper}>
              <View style={{ flex: 1 }}>
                <View style={{ minHeight: 60 }}>
                  <Text numberOfLines={3} style={styles.title}>
                    {item.title}
                  </Text>
                </View>
                <Text numberOfLines={3} style={styles.subtitle}>
                  {item.description}
                </Text>
              </View>
            </View>

            <View style={styles.footer}>
              <View>
                <Text style={styles.author}>Author: {item.author}</Text>
                <Text style={styles.date}>{created_at_format}</Text>
              </View>
            </View>
          </View>
          <View style={styles.sourceWrapper}>
            <Text style={styles.sourceText}>
              {item.source.name ? item.source.name : '-'}
            </Text>
          </View>
          <FastImage style={styles.image} source={{ uri: imageUrl }} />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />
        <View style={styles.navbar}>
          <Text style={styles.headline}>{appString.pages.home.title}</Text>
          <Pressable
            onPress={() => {
              navigation.navigate('Search');
            }}>
            <View style={{ padding: 5 }}>
              <FontAwesome5 name={'search'} color={'black'} size={20} />
            </View>
          </Pressable>
        </View>
        <View>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{ marginBottom: 10 }}>
            {dataCategory.map((item, index) => {
              let activeColor = 'transparent';
              if (item === selectedCategory) {
                activeColor = Color.primaryColor;
              }
              return (
                <Pressable
                  key={index}
                  onPress={() => {
                    setSelectedCategory(item);
                  }}>
                  <View
                    style={{
                      padding: 15,
                      // backgroundColor: activeColor,
                      marginLeft: 15,
                      borderBottomColor: activeColor,
                      borderBottomWidth: 3,
                    }}>
                    <Text>{item.toUpperCase()}</Text>
                  </View>
                </Pressable>
              );
            })}
          </ScrollView>
        </View>

        {dataNews.length > 0 ? (
          <FlatList
            data={dataNews}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={renderFooter}
            onMomentumScrollBegin={() => {
              onEndReachedCalledDuringMomentum = false;
            }}
            onEndReached={() => {
              if (!onEndReachedCalledDuringMomentum) {
                getNews({ isReset: false });
                onEndReachedCalledDuringMomentum = true;
              }
            }}
            onEndReachedThreshold={0.2}
          />
        ) : (
          Array.from({ length: 10 }).map((_, index) => (
            <View key={index} style={{ marginBottom: 15, padding: 10 }}>
              <SkeletonPlaceholder>
                <SkeletonPlaceholder.Item flexDirection="row">
                  <SkeletonPlaceholder.Item
                    flex={1}
                    paddingHorizontal={10}
                    justifyContent={'space-between'}>
                    <SkeletonPlaceholder.Item
                      width="100%"
                      height={180}
                      borderRadius={6}
                    />
                  </SkeletonPlaceholder.Item>
                </SkeletonPlaceholder.Item>
              </SkeletonPlaceholder>
            </View>
          ))
        )}
      </View>
    </SafeAreaView>
  );
};

export default HomePage;

/* eslint-disable react-native/no-inline-styles */
import { Button } from 'components';
import {
  React,
  Text,
  View,
  Image,
  TextInput,
  Dimensions,
  ScrollView,
  Pressable,
  useState,
} from 'libraries';
import { appString } from 'utils';
import styles from './style';
import API from 'configs/api';

// class RegisterPage extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       name: '',
//       email: '',
//       password: '',
//       age: '',
//     };
//   }

const RegisterPage = (props) => {
  const { navigation } = props;

  const [state, setState] = useState({
    name: '',
    email: '',
    password: '',
    age: '',
  });

  const submitRegister = async () => {
    const { name, email, password, age } = state;
    const payload = {
      body: {
        name,
        email,
        password,
        age,
      },
    };
    try {
      console.log('registering...');
      const reqData = await API.register(payload);
      if (reqData.token) {
        alert('Register Success');
        navigation.navigate('Login');
      } else {
        alert('Register Failed');
      }
      console.log(reqData);
    } catch (e) {
      alert('Error: ', e);
      console.log(e);
    }
  };

  // render() {
  const title = appString.pages.register.title;
  const footer1 = appString.pages.register.footer1;
  const footer2 = appString.pages.register.footer2;
  const backgroundImageUrl = 'https://wallpaperaccess.com/full/2919081.jpg';
  const logoUrl =
    'https://raw.githubusercontent.com/fabiospampinato/vscode-todo-plus/master/resources/logo/logo.png';
  return (
    <View style={styles.container}>
      <Image
        style={styles.backgroundImage}
        source={{
          uri: backgroundImageUrl,
        }}
      />
      <ScrollView>
        <View style={styles.topWrapper}>
          <Image
            style={styles.logo}
            source={{
              uri: logoUrl,
            }}
          />
          <Text style={styles.text.title}>{title}</Text>
          <TextInput
            placeholder="Nama Lengkap"
            style={{
              height: 60,
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              width: Dimensions.get('window').width * 0.8,
            }}
            onChangeText={(text) =>
              setState({
                ...state,
                name: text,
              })
            }
          />
          <TextInput
            placeholder="Email"
            style={{
              height: 60,
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              width: Dimensions.get('window').width * 0.8,
            }}
            onChangeText={(text) =>
              setState({
                ...state,
                email: text,
              })
            }
          />

          <TextInput
            placeholder="Kata Sandi"
            secureTextEntry={true}
            style={{
              height: 60,
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              width: Dimensions.get('window').width * 0.8,
            }}
            onChangeText={(text) =>
              setState({
                ...state,
                password: text,
              })
            }
          />

          <TextInput
            placeholder="Age"
            style={{
              height: 60,
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              width: Dimensions.get('window').width * 0.8,
            }}
            onChangeText={(text) =>
              setState({
                ...state,
                age: text,
              })
            }
          />
        </View>

        <View style={styles.bottomWrapper}>
          <Button
            title={appString.general.button.register.toUpperCase()}
            onPress={() => submitRegister()}
          />
          <Text style={styles.text.footer1}>{footer1}</Text>
          <Pressable onPress={() => navigation.navigate('Login')}>
            <Text style={styles.text.footer2}>{footer2}</Text>
          </Pressable>
        </View>
      </ScrollView>
    </View>
  );
  // }
};

export default RegisterPage;

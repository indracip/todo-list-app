/* eslint-disable react-native/no-inline-styles */
import { Button } from 'components';
import { React, Text, View, Image, ScrollView, StatusBar } from 'libraries';
import { appString } from 'utils';
import styles from './style';

// class LandingPage extends React.Component {
//   render() {
const LandingPage = ({ navigation }) => {
  // const { navigation } = this.props;
  const title = appString.pages.landing.title;
  const subtitle = appString.pages.landing.subtitle;
  const backgroundImageUrl = 'https://wallpaperaccess.com/full/2919081.jpg';
  const logoUrl =
    'https://raw.githubusercontent.com/fabiospampinato/vscode-todo-plus/master/resources/logo/logo.png';
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={'#F4F4F4'} barStyle={'dark-content'} />
      <Image
        style={styles.backgroundImage}
        source={{
          uri: backgroundImageUrl,
        }}
      />
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.topWrapper}>
          <Image
            style={styles.logo}
            source={{
              uri: logoUrl,
            }}
          />

          <Text style={styles.text.title}>{title}</Text>
          <Text style={styles.text.subtitle}>{subtitle}</Text>
        </View>
        <View style={styles.bottomWrapper}>
          <Button
            title={appString.general.button.login.toUpperCase()}
            onPress={() => navigation.navigate('Login')}
          />

          <Button
            title={appString.general.button.register.toUpperCase()}
            onPress={() => navigation.navigate('Register')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default LandingPage;

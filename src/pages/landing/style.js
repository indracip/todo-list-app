import { scale } from 'utils';

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute',
    zIndex: 0,
    width: '100%',
    height: '100%',
  },
  topWrapper: {
    flex: 1,
    marginTop: scale(150),
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  logo: {
    backgroundColor: 'white',
    borderRadius: 25,
    height: scale(150),
    width: scale(150),
    marginBottom: 15,
  },
  text: {
    title: {
      marginTop: 10,
      color: 'black',
      fontSize: scale(30),
      fontWeight: 'bold',
    },
    subtitle: {
      marginTop: 20,
      color: 'black',
      fontSize: scale(18),
      textAlign: 'center',
    },
  },

  bottomWrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 50,
  },
};

export default styles;

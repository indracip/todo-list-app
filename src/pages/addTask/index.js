import React from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import { Button, Text } from 'components';
import { FONTS, scale } from 'utils';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import { useState, useEffect } from 'libraries';
import { Icon } from 'react-native-eva-icons';
import { addTask, updateStatus } from 'configs/redux/actions/taskActions';
import { connect } from 'react-redux';
import { showMessage, hideMessage } from 'react-native-flash-message';

const AddTaskPage = (props) => {
  const {
    navigation,
    dispatchAddTask,
    loadingAdd,
    successAdd,
    dispatchUpdateStatus,
  } = props;
  const [textData, setTextData] = useState('');

  useEffect(() => {
    if (successAdd == true) {
      showMessage({
        message: 'Success',
        description: 'Your task has been created',
        type: 'success',
      });
      setTimeout(function () {
        hideMessage();
        console.log('udh 3 detik');
      }, 3000);
      navigation.pop();
    } else if (successAdd == false) {
      showMessage({
        message: 'Ups',
        description: 'Sorry, updating task failed',
        type: 'danger',
      });
      navigation.pop();
    }
    dispatchUpdateStatus('add', null);
  }, [successAdd]);

  const submitTask = () => {
    try {
      const payload = {
        body: {
          description: textData,
        },
      };
      dispatchAddTask(payload);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View
      style={{
        flex: 1,
        paddingHorizontal: scale(15),
        backgroundColor: '#FBFAFF',
      }}>
      <View style={{ position: 'relative' }}>
        <Text
          style={{
            fontSize: scale(25),
            alignSelf: 'center',
          }}>
          New Task
        </Text>

        <View style={{ position: 'absolute', right: 0, top: 0 }}>
          <TouchableOpacity
            onPress={() => {
              console.log('press');
              navigation.pop();
            }}>
            <Icon name="close" width={40} height={40} fill={'gray'} />
          </TouchableOpacity>
        </View>
      </View>

      <Text
        style={{
          marginTop: scale(24),
          fontSize: scale(18),
          marginBottom: scale(14),
        }}>
        What are you planning ?
      </Text>
      <View
        style={{
          paddingLeft: 10,
          marginBottom: scale(24),
          backgroundColor: 'white',
          borderWidth: 0,
          borderRadius: 10,
          minHeight: scale(200),
        }}>
        <AutoGrowingTextInput
          value={textData}
          onChange={(event) => {
            setTextData(event.nativeEvent.text);
          }}
          placeholder={'Your Message'}
          placeholderTextColor="#66737C"
          style={{
            fontFamily: FONTS.quickSand,
            fontSize: scale(15),
          }}
          enableScrollToCaret
        />
      </View>
      <Button
        title={'Submit'}
        backgroundColor={'#24B07D'}
        textColor={'white'}
        isLoading={loadingAdd}
        loadingColor={'white'}
        onPress={() => {
          submitTask();
        }}
      />
    </View>
  );
};

function mapStateToProps(state) {
  return {
    loadingAdd: state.taskStore.loadingAdd,
    successAdd: state.taskStore.successAdd,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchAddTask: (payload) => dispatch(addTask(payload)),
    dispatchUpdateStatus: (type, payload) =>
      dispatch(updateStatus(type, payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTaskPage);

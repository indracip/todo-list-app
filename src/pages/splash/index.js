import React from 'react';
import { View, Text, useEffect, AsyncStorage } from 'libraries';

const SplashScreen = ({ navigation }) => {
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    AsyncStorage.getItem('token', (error, result) => {
      if (result) {
        navigation.navigate('Welcome');
      } else {
        navigation.navigate('Landing');
      }
    });
  };
  return (
    <View>
      <Text>Ini Splash Screen</Text>
    </View>
  );
};

export default SplashScreen;

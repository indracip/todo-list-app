import HomePage from './home';
import SearchPage from './search';
import NewsDetail from './newsDetail';
import AllTaskPage from './allTask';
import ActiveTaskPage from './activeTask';
import CompletedTaskPage from './completedTask';
import ProfilePage from './profile';
import AddTaskPage from './addTask';
import LandingPage from './landing';
import LoginPage from './login';
import RegisterPage from './register';
import SplashScreen from './splash';

export {
  HomePage,
  SearchPage,
  NewsDetail,
  AllTaskPage,
  ActiveTaskPage,
  CompletedTaskPage,
  ProfilePage,
  AddTaskPage,
  LandingPage,
  LoginPage,
  RegisterPage,
  SplashScreen,
};
